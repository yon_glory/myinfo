import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/internal/Observable';
import { ENV_DATA } from './model';
@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
  GetPersonalData(authCode): Observable<any> {
    const url = `${environment.apiUrl}/api/integrate/GetPersonData`;
    const data = {
      "keyword": authCode,
      "attributes": ENV_DATA.attributes
    }
    return this.http.post(url, data);
  }

}

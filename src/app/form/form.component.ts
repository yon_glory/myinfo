import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ApiService } from '../api.service';
import { ENV_DATA } from '../model';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { Router, NavigationStart, Event as NavigationEvent, ActivatedRoute, ParamMap, UrlTree, UrlSegment, UrlSegmentGroup } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  profileForm = this.fb.group({
    vehicleno: ['']
  });
  constructor(private fb: FormBuilder, public apiService: ApiService,
    public router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    const formarray = ENV_DATA.attributes.split(',');
    for (let i = 0; i < formarray.length; i++) {
      this.profileForm.addControl(formarray[i], new FormControl([""]));
    }
    // console.log(this.router.url)
    if (this.router.url.indexOf("callback") > -1) {
      const parseUrl = this.router.parseUrl(this.router.url);
      if (parseUrl.queryParamMap["params"]) {
        const authCode = parseUrl.queryParamMap["params"].code;
        this.callServerAPIs(authCode);
      }
    }
  }
  submit() {
    console.log(this.profileForm.value)
  }

  callAuthoriseApi() {
    const authoriseUrl = ENV_DATA.authApiUrl + "?client_id=" + ENV_DATA.clientId +
      "&attributes=" + ENV_DATA.attributes +
      "&purpose=" + ENV_DATA.purpose +
      "&state=123" +
      "&redirect_uri=" + ENV_DATA.redirectUrl;

    window.location.href = authoriseUrl;
  }
  callServerAPIs(authCode) {
    if (authCode) {
      this.apiService.GetPersonalData(authCode)
        .toPromise()
        .then(res => {
          console.log(res)
          if (res.data) {
            for (const key in this.profileForm.controls) {
              for (const resultkey in res.data) {
                if (key === resultkey) {
                  if (key === "cpfbalances") {
                    this.profileForm.get(resultkey).setValue(this.getValue(res.data[resultkey].oa));
                  }
                  else this.profileForm.get(resultkey).setValue(this.getValue(res.data[resultkey]));
                } else if (resultkey === "vehicles") {
                  this.profileForm.get("vehicleno").setValue(this.getValue(res.data.vehicles[0].vehicleno));
                }
              }
            }
            console.log(this.profileForm)
          } else {
            alert(res.errors[0].title)
          }
        })
    }
  }
  getValue(data) {
    if (!data)
      return null;
    if (data.value)
      return data.value;
    else if (data.desc)
      return data.desc;
    else if (typeof data == "string")
      return data;
    else
      return "";
  }
}


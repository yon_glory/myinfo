export const ENV_DATA: any = {
    attributes: "uinfin,name,sex,race,nationality,dob,email,mobileno,regadd,housingtype,hdbtype,marital,edulevel,noa-basic,ownerprivate,cpfcontributions,cpfbalances",
    authApiUrl: "https://sandbox.api.myinfo.gov.sg/com/v3/authorise",
    authLevel: "L0",
    clientId: "STG2-MYINFO-SELF-TEST",
    redirectUrl: "http://localhost:3001/callback",
    status: "OK",
}
//vehicles.vehicleno,